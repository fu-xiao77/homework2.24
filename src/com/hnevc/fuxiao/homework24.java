package com.hnevc.fuxiao;
/**
 *
 * 回文数的和
 * 所谓回文数是从左至右与从右至左读起来都是一样的数字，如：121 是一个回文数。
 * 以下编程是求出 100—200 的范围内所有回文数的和
 */
public class homework24 { 
    public static void main(String[] args) {
    String str = " ";
    for (int i = 100; i <= 200; i++) {
        if (i % 10 == i / 100)
            System.out.print(i + " ");
    }
}
}
